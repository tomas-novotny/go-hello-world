package main

import (
    "net/http"
    "log"
    "html/template"
    "path/filepath"
    "os"
)

func main() {
    fs := http.FileServer(http.Dir("static"))
    http.Handle("/static/", http.StripPrefix("/static/", fs))

    http.HandleFunc("/", serveTemplate)

    log.Println("Listening...")
    err := http.ListenAndServe(":3300", nil)
    if err != nil {
        log.Println(err.Error())
    }
}

func serveTemplate(w http.ResponseWriter, r *http.Request) {
    layoutTemplate := filepath.Join("templates", "_layout", "master.html")
    headTemplate := filepath.Join("templates", "_layout", "head.html")
    scriptsTemplate := filepath.Join("templates", "_layout", "scripts.html")
    headerTemplate := filepath.Join("templates", "_layout", "header.html")
    footerTemplate := filepath.Join("templates", "_layout", "footer.html")
    pageTemplate := filepath.Join("templates", filepath.Clean(r.URL.Path)+".html")

    // Return a 404 if the template doesn't exist
    info, err := os.Stat(pageTemplate)
    if err != nil {
        if os.IsNotExist(err) {
            http.NotFound(w, r)
            return
        }
    }

    // Return a 404 if the request is for a directory
    if info.IsDir() {
        http.NotFound(w, r)
        return
    }

    tmpl, err := template.ParseFiles(layoutTemplate, headTemplate, headerTemplate, footerTemplate, scriptsTemplate, pageTemplate)
    if err != nil {
        // Log the detailed error
        log.Println(err.Error())
        // Return a generic "Internal Server Error" message
        http.Error(w, http.StatusText(500), 500)
        return
    }

    if err := tmpl.ExecuteTemplate(w, "layout", nil); err != nil {
        log.Println(err.Error())
        http.Error(w, http.StatusText(500), 500)
    }
}
